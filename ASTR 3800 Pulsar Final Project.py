# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 09:21:15 2015

@author: Jacob
"""

import numpy as np
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D


# Note: data has been edited to only include full datasets, some information was invalid
dataN= np.genfromtxt('C:\Users\Jacob\Documents\Python Scripts\Pulsar.tsv', comments = '#', delimiter=';')
per1 = dataN[:,0] #ms
Spindown = dataN[:,1] #erg/s
D1 = dataN[:,2] #kpc
Derr = dataN[:,3] #^error
Lum1 = dataN[:,4] #erg/s
Lumerr = dataN[:,5] #^stat error
Lumserr = dataN[:,6] #^sys error
RFlux = dataN[:,7] #10e-26 W/(m**2*Hz)
RFlux2 = dataN[:,8] #10e-26 W/(m**2*Hz)
alpha1 = dataN[:,9] #N/A
alphaerr = dataN[:,10] #^error
EFlux1 = dataN[:,11] #mW/(m**2)
EFluxerr = dataN[:,12] #^error

#conversions, if neccessary
EFlux = EFlux1*1E3 #mW to W
D = D1*3.08567758E19 #kpc to m
Lum = Lum1*1E-7 #erg to J
per = per1*1E3 #ms to s
fre = 1/(per1*1E3) #ms to 1/s

def fitter(x,y, n = 1):
    """This function will perform a linear least squares fit on the given values

    Input:
    x = The independent value.
    y = The dependent value
    n = An optional parameter for the degree of the polynomial being fit

    Output:
    b = The coefficients of the fit, [constant, slope]
    fit = The dependent values estimated based on the fitted coefficients.
    fit_err = The error in the fitted values.
    """

    # Composing the array of x values in the form [1 x x^2...x^n] where each value in that 
    #   list implies a column (first a column of ones, then x, etc.)
    X = np.array([x**i for i in range(n+1)])

    # Calculating the coefficients of the polynomial with the linear least squares solution
    b = np.dot(np.linalg.inv(np.dot(X,X.T)),np.dot(y,X.T))


    # Calculating the fitted line and estimating the error in the fit
    fit = np.dot(b,X)
    res = (y-fit)**2
    fit_err =  np.sqrt(res.sum())/len(y) # error = L2 Norm


    return b, fit, fit_err
#%%
#first let's see what the 3 relationships look like together, it's AWESOME.
#define 3D parameter space for some variables within my shitload of data
fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(per, Spindown, Lum1, color = 'red')
fig.savefig('3D model!') #sweet this looks like things I can do fits with!


#%%
#using Taylor's Linear fit method, we find the constants to fulfill Bx+A
A = (np.sum(per**2)*np.sum(Spindown)-np.sum(per)*np.sum(per*Spindown))/(len(per)*np.sum(per**2)-np.sum(per)**2)
B = (len(per)*np.sum(per*Spindown)-np.sum(per)*np.sum(Spindown))/(len(per)*np.sum(per**2)-np.sum(per)**2)

line = B*per + A
pl.plot(per, line, 'r', label= 'Linear Fit')
pl.plot(per, Spindown, 'k.')
pl.title('This Is a Terrible Fit')
pl.xlabel('Period [s]')
pl.ylabel('Spindown [erg/s]')
pl.legend()

#%%
#Okay so after some googling, there's a matrix method for higher powers which we did in tutorial
#fitter taken from tutorial notes

#Now we use fitter and 2nd paower, notice the difference!
fit = fitter(per, Spindown, 2)
pl.plot(per, fit[1], 'r', label = 'Quadratic Fit')
pl.plot(per, Spindown, 'k.')
pl.title('Period vs. Spindown')
pl.xlabel('Period [s]')
pl.ylabel('Spindown [erg/s]')
pl.legend()
#Okay that's great, it is a parabola. Now let's keep adding powers until it breaks.

#%%
fit = fitter(per, Spindown, 3)
pl.plot(per, fit[1], 'r', label = 'Cubic Fit')
pl.plot(per, Spindown, 'k.')
pl.title('Period vs. Spindown')
pl.xlabel('Period [s]')
pl.ylabel('Spindown [erg/s]')
pl.legend()
#So I sent this up to the 10th power and it started turning into a Sinusoidal, which is okay but too complicated for it to make any sense
#the 3rd power is the keeper because it give the extrema in a more accurate region to where the data lies

#%%
fit1 = fitter(per, Lum1, 3)
pl.plot(per, fit1[1], 'r', label = 'Cubic Fit')
pl.plot(per, Lum1, 'k.')
pl.title('Period vs. Luminosity')
pl.xlabel('Period [s]')
pl.ylabel('Luminosity [erg/s]')
pl.legend()
#again, the 3rd power was ideal to put the extrema in the appropriate region

#%%
fit2 = fitter(Spindown, Lum1, 1)
pl.plot(Spindown, fit2[1], 'r', label = 'Linear Fit')
pl.plot(Spindown, Lum1, 'k.')
pl.title('Spindown vs. Luminosity')
pl.xlabel('Spindown [erg/s]')
pl.ylabel('Luminosity [erg/s]')
pl.legend()
#this one breaks after a linear fit, which makes sense because these are rather nonsenseable to be plotting against eachother

#%%
#now for making some lines to go plot a parametrized surface
#first we need to make damn lines

#print the constants to use in the space. I could have put in the fit locations to the code,
#but I was curious to see what the numbers actually are
print fit1[0][0], fit1[0][1], fit1[0][2], fit[0][3]
print fit[0][0], fit[0][1], fit[0][2], fit[0][3]

#creating a plane equation that takes the cubic LoBF 
#also, these steps follow how every online tutorial makes surfaces with x,y functions below
#linspace is using the extrema from Lum1 and per
x = np.outer(np.linspace( 1750,350000 , 85), np.ones(len(per)))
y = np.outer(np.ones(len(per)), np.linspace(0.2,17,85))
z = 39 + -0.00052*x + -8.15E-10*x**2 + 7.37E-15*x**3 + 6.68E-6*y + 6.61E-11*y**2 + 7E-15*y**3
#%%
#Nailed it.
fig = pl.figure()
ax = fig.add_subplot(111, projection='3d') #set up things
pl.locator_params(axis = 'x', nbins = 7) #my period axis was very clutter, this fixes that
ax.plot_surface(x,y,z, alpha=0.3) #plotting the lines above
ax.scatter(per, Lum1, Spindown, color = 'red') #same plot in the first 3d plot, but now with the space overlayed
ax.set_xlabel('Period [s]')
ax.set_ylabel('Luminosity [erg/s]')
ax.set_zlabel('Spindown [erg/s]')
ax.text2D(0.05, 0.95, "Parametrized Space", transform=ax.transAxes)
pl.savefig('Griddy')
