You have until next Sunday at noon to correct your homework as you see fit.

Code for:			Score:	Comments:
HW2 (plotting points)		10/10	Totes good.

HW3 (blackbody plot of stars)	8/10	-You need to use r in your Star 
    	       	       			 function, you aren't in units of flux 
					 right now.
					-You also shouldn't normalize your 
					 function, try just plotting stars at
					 similar distance/brightnesses.
					-Please include a more detailed legend.

HW4 (binning blackbody)		2/5 	-Your luminosity is super off
    	     				-Still no surface area (these two
					 comments are connected)
					-That is not binning. Look at 
					 binning_ex.pdf on the class repo.

HW4 (Gaussian populations)	3/5	-You need to use the erf function

HW5 (Hubble blackbody)          0/10    -???

HW6 (Rays and xls)              6/10    -Write the file
    	      				-Where is your xls plot?

HW7 (Correlation)               6/10    -Need A/B/r/confidence on the plot
    					-Find r and confidence (try
					 scipy.stats.pearsonr)

HW8 (M33)                       10/10    -Noice.

HW10 (Spectrum analysis)        7/10     -Sweet, now marginalize to find 
     	       				  the good temperature range.


Note:
I'll change your grades as things get updated on Bit, so don't quake at the zeroes

Also, it is incorrect to upload things to your downloads folder. If you need help using bit, let me walk you through it in class or over email.
